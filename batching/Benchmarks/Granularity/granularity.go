package main

import (
	"fmt"
	"math"
	"os"
	"runtime"
	"runtime/trace"
	"strconv"
	"time"
)

const (
	l = 4096
	t = true // True => time, False => trace
)

func work(i, g int, a []int) {
	for k := 0; k < g; k++ {
		a[g*i+k]++
	}
}

func worker(i, g int, a []int, done chan bool) {
	for j := 0; j < l; j++ {
		work(i, g, a)
	}
	done <- true
}

func main() {

	if len(os.Args) != 5 {
		panic("Check args")
	}

	is, _ := strconv.Atoi(os.Args[1])
	ig, _ := strconv.Atoi(os.Args[2])
	p, _ := strconv.Atoi(os.Args[3])
	r, _ := strconv.Atoi(os.Args[4])

	done := make(chan bool)

	s := int(math.Pow(2, float64(is)))
	a := make([]int, s) // Set array size

	// Set core count
	runtime.GOMAXPROCS(p)
	if curp := runtime.GOMAXPROCS(0); curp != p {
		print("current: ", curp, ", set: ", p, "\n")
		panic("GOMAXPROCS() failed")
	}

	g := int(math.Pow(2, float64(ig))) // Set granularity

	// bsz := uint((s / g) / (p * p))
	// ysz := uint(bsz * l)

	// runtime.GOBATCHSIZE(bsz)
	// runtime.GOYIELDRATE(ysz)

	var tm time.Time

	if !t {
		// Setup trace
		f, err := os.Create("trace[" + strconv.Itoa(s) + ", " + strconv.Itoa(g) + ", " + strconv.Itoa(p) + "].out")
		if err != nil {
			panic(err.Error())
		}
		defer f.Close()

		trace.Start(f) // Start trace
	} else {
		fmt.Printf("%d, %d, %d, %d,", s, g, p, r)
		tm = time.Now() // Start Run
	}

	for i := 0; i < s/g; i++ {
		go worker(i, g, a, done)
	}
	for i := 0; i < s/g; i++ {
		<-done
	}

	if !t {
		trace.Stop() // Stop trace
	} else {
		d := time.Since(tm) // End Run
		fmt.Printf(" %f\n", float32(d)/1000000000)
	}

	for _, x := range a { // Check and Reset
		if x != l {
			panic(x)
		}
	}
}
