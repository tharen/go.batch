import re, csv
from itertools import groupby
from statistics import mean, variance
import matplotlib.pyplot as plt

font = {'fontname':'Latin Modern Math'}
family = {'family':'Latin Modern Math'}

# gmrk = {2:".", 4:"1", 8:"x", 16:"|", 32:"v", 64:9, 128:"^", 256:"$o$", 1024:"D", 4096:"s"}
pmrk = {1:"x", 2:"1", 3:"|", 4:"."}

binfile = "./granularity.batch.bnch.txt"
boutfile = "./granularity.batch.bnch.csv"
bfiles = (binfile, boutfile)

goinfile = "./granularity.go.bnch.txt"
gooutfile = "./granularity.go.bnch.csv"
gofiles = (goinfile, gooutfile)

def load(file):
    inputfile = open(file, "r", newline='')
    rfile = inputfile.read()[:-1]
    inputfile.close()

    data = []
    for line in rfile.split('\n'):
        splitline = line.split(', ')
        
        s = int(splitline[0])
        g = int(splitline[1])
        p = int(splitline[2])
        r = int(splitline[3])
        t = float(splitline[4])
        
        value = (s, g, p, r, t)
        data.append(value)

    return data

def save(out, header, file):
    fh = open(file, "w")
    fh.write(header)
    for line in out:
        fh.write("%s\n" % str(line)[1:-1])
    fh.close() 

def compute(data):
    computed = []
    for key, group in groupby(data, key=lambda p: p[0:3]):
        glist = list(group)
        times = [r[4] for r in glist]

        m = mean(times)
        v = variance(times)
        
        k = list(key)
        k.extend([m, v])
        computed.append(tuple(k))

    return computed

def pattern(data):
    small = 65536
    large = 1048576

    bdata = data[1]

    def fill(filtr):
        pdict = dict()
        for _, g, p, t, _ in list(filtr):
            if p not in pdict:
                pdict[p] = {}
            pdict[p][g] = t
        return pdict

    plots = dict()
    bsmall = fill(filter(lambda r: r[0] == small, bdata))
    plots[1] = (bsmall, small, "Batched")
    blarge = fill(filter(lambda r: r[0] == large, bdata))
    plots[2] = (blarge, large, "Batched")

    for i in range(1, 2+1):
        dt = plots[i][0]
        sz = plots[i][1]
        sched = plots[i][2]

        plt.subplot(2,1,i)
        plt.title(sched + ": Array Size (s) "+str(sz)+" words, process length (l) 4096", **font)
        
        for p in dt:
            gran = [g for g in dt[p]]
            time = [dt[p][g] for g in gran]
            plt.plot(gran, time, color='k', label=str(p) + " Processor(s)", linewidth=.5, marker=pmrk[p])
        
        plt.ylabel('Time (s)', **font)
        plt.xlabel('g - Granularity', **font)
        plt.yticks(**font)
        plt.xticks(**font)
        plt.legend(prop=family, fancybox=False, shadow=True, edgecolor="inherit")

    plt.show()

def performance(data):
    glim = 64
    small = 65536
    large = 1048576

    godata = data[0]
    bdata = data[1]

    def fill(filtr):
        pdict = dict()
        for _, g, p, t, _ in list(filtr):
            if p not in pdict:
                pdict[p] = {}
            pdict[p][g] = t
        return pdict

    plots = dict()
    bgsmall = fill(filter(lambda r: r[0] == small and r[1] <= glim, bdata))
    plots[1] = [(bgsmall, small, "Batched", '#CE3262')]
    gosmall = fill(filter(lambda r: r[0] == small and r[1] <= glim, godata))
    plots[1].append((gosmall, small, "Go", '#00ADD8'))
    
    bglarge = fill(filter(lambda r: r[0] == large and r[1] <= glim, bdata))
    plots[2] = [(bglarge, large, "Batched", '#CE3262')]
    golarge = fill(filter(lambda r: r[0] == large and r[1] <= glim, godata))
    plots[2].append((golarge, large, "Go", '#00ADD8'))

    for i in range(1, 3):
        
        sz = plots[i][1][1]
        plt.subplot(1,2,i)
        plt.title("Array Size (s) "+str(sz)+" words, process length (l) 4096", **font)
        
        for pl in plots[i]:
            dt = pl[0]
            sched = pl[2]
            col = pl[3]

            for p in dt:
                gran = [g for g in dt[p]]
                time = [dt[p][g] for g in gran]
                plt.plot(gran, time, color=col, label=sched+": "+str(p) + " Processor(s)", linewidth=.5, marker=pmrk[p])
            

        plt.ylabel('Time (s)', **font)
        plt.xlabel('g - Granularity', **font)
        plt.yticks(**font)
        plt.xticks(**font)
        plt.legend(prop=family, fancybox=False, shadow=True, edgecolor="inherit")

    plt.show()

def computespeedup(data):
    speedup = dict()    
    for key, group in groupby(data, key=lambda x: x[1]):
        # print(key, glist)
        glist = list(group)
        if key not in speedup:
            speedup[key] = {}
        speedup[key][glist[0][2]] = glist[0][3]

    for g in speedup:
        t1 = speedup[g][1]
        for procs in speedup[g]:
            speedup[g][procs] = t1/speedup[g][procs]
    
    return speedup

def speedup(data):
    godata = list(filter(lambda r: r[0] == 1048576, data[0]))
    bdata = list(filter(lambda r: r[0] == 1048576, data[1]))
    
    proccounts = list({r[2] for r in godata})
    # proccounts.remove(3)
    large = 1048576
    plots = {1:(godata, "Go"), 2:(bdata, "Batched")}

    for i in range(1, 3):
        
        dt = plots[i][0]
        name = plots[i][1]

        plt.subplot(2,1,i)

        ## Linear Speedup
        plt.plot(proccounts, proccounts, color='k', label="Linear Speedup")
        
        speedup = computespeedup(dt)
        for g in speedup:
            # speedup[g].pop(3, None)
            plotspeed = [speedup[g][s] for s in speedup[g]]
            plt.plot(proccounts, plotspeed, label="g - "+ str(g), linewidth=.5, marker='.')
        
        plt.title(name+" Speedup with array size (s) "+str(large)+" words, process length (l) 4096", **font)
        plt.ylabel('Speedup T(1)/T(P)', **font)
        plt.xlabel('Processors', **font)
        plt.yticks(**font)
        plt.xticks(proccounts, **font)
        plt.legend(prop=family, fancybox=False, shadow=True, edgecolor="inherit")

    plt.show()

data = []
for filetype in [gofiles, bfiles]:
    readings = load(filetype[0])
    computed = compute(readings)
    save(computed, "arraysize, granularity, procs, mean time(s), variance\n", filetype[1])
    data.append(computed)

pattern(data)
performance(data)
speedup(data)
