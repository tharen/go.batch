#!/bin/bash
fyp=../..
gobatch=$fyp/go.batch/bin/go

cd $fyp/Benchmarks/Granularity
go build ./granularity.go
#$gobatch build ./granularity.go

filename=g.0.4.txt
> $filename

echo "s, g, p, time"

for s in 20
do
for p in 4
do
for g in 1 2 3 4 5 6 7 8 10 12
do
for (( r=1; r<=5; r++ ))
do 
	echo $s, $g, $p, $r
	./granularity $s $g $p $r >> $filename
done
done
done
done
