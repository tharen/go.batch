import re
from parse import parse
from statistics import mean
import matplotlib.pyplot as plt

font = {'fontname':'Latin Modern Math'}
family = {'family':'Latin Modern Math'}

def loadbatched():
    bsz = 26
    ysz = 16
    diff = bsz-ysz
    expectedreadings = (bsz*(bsz+1) - (diff)*(diff+1))/2
    filename = "./Batched/commstime.batch.bnch.txt"
    
    inputfile = open(filename, "r")
    rfile = inputfile.read()
    inputfile.close()

    stats = re.findall(r"STATS.*", rfile)
    
    bstats = {"best":dict(), "avg":dict(), "worst":dict()}

    for s in stats:
        parsed = parse("STATS for p:{}\tBest:({},{},{}), AVG({}, {}), Worst({},{},{})", s)

        p = int(parsed[0])
        best = float(parsed[1])
        bb = int(parsed[2])
        yb = int(parsed[3])
        avg = float(parsed[4])
        varratio = (float(parsed[5])*100)/expectedreadings
        worst = float(parsed[6])
        bw = int(parsed[7])
        yw = int(parsed[8])

        bstats["best"][p] = (best, bb, yb)
        bstats["avg"][p] = (avg, varratio)
        bstats["worst"][p] = (worst, bw, yw)
    
    return bstats

def loadgo():
    filename = "./Go/commstime.go.bnch.txt"
    inputfile = open(filename, "r")
    rfile = inputfile.read()
    inputfile.close()

    # Split on benchmark completion
    p = re.compile(r"PASS\n.*\n?")
    runs = list(filter(None, p.split(rfile)))
    # printer(runs)

    # Remove benchmark start and last line
    clean = list(map(lambda run: run.split('\n')[2:-1], runs))
    # printer(clean)

    return clean

def formatgo(lines):
    formatted = {}
    parsed = []
    for _, run in enumerate(lines):
        for n, read in enumerate(run):
            nl = read
            
            # For single core, imply 1
            if "-" not in nl:
                nl = nl.replace("BenchmarkCommstime", "1", 1)
            else: # Distinguish Core Count
                nl = nl.replace("BenchmarkCommstime-", "", 1)

            # Remove Whitespace for repeat count and remove ns/op
            nl = re.sub(
                r"(\d*)(\s*\d*\s*)(\d*)(\sns/op)",\
                r"\1, \3", nl)

            # Add Entry for Run
            nl = re.sub(r"(\d*)(\s)(\d*)",\
                r"\1 R, \3", nl)

            # Replace run entry with value
            nl = nl.replace("R", str(n+1), 1)

            # Replace time in ns to seconds
            value = re.search(r"(\d*)$", nl).group(0)
            seconds = str(int(value))
            nl = nl.replace(value, seconds, 1)
            
            p = parse("{}, {}, {}", nl)
            parsed.append(tuple(p))

    procs = {int(p[0]) for p in parsed}

    for p in procs:
        ptimes = [int(tupl[2]) for tupl in list(filter(lambda r: int(r[0]) == p, parsed))]
        formatted[p] = mean(ptimes)

    return formatted

def graph(g, b):
    procs = [p for p in g]
    maxprocs = len(g)

    plt.title("CommsTime Performance (Lower is Better)", **font)
    plt.ylabel('Time (ns/op)', **font)
    plt.xlabel('Processors', **font)
    plt.yticks(**font)
    plt.xticks(procs, **font)
    
    # Go CommsTime
    times = [g[p] for p in g]
    plt.plot(procs, times, color='k', label="Go Performance", linewidth=1.5)

    # Batched Best Commstime
    bb = b["best"]
    times = [bb[p][0] for p in bb]
    plt.plot(procs, times, color='k', label="Best Batched Performance", marker="P", linewidth=1.25)

    # Batched Average Commstime
    bavg = b["avg"]
    times = [bavg[p][0] for p in bavg]
    plt.plot(procs, times, color='k', label="Average Batched Performance", marker="x", linewidth=.5)

    # Batched Worst Commstime
    bw = b["worst"]
    times = [bw[p][0] for p in bw]
    plt.plot(procs, times, color='k', label="Worst Batched Performance", marker="^", linewidth=.5)
    
    plt.legend(prop=family, fancybox=False, shadow=True, edgecolor="inherit")
    plt.show()

def printstats(g, b):
    
    procs = [p for p in b["avg"]]
    
    for p in procs:
        t = g[p]
        print("%d, %.1f" % (p, t))

    print()
    
    for p in procs:
        tb = b["best"][p][0]
        bb = b["best"][p][1]
        yb = b["best"][p][2]
        tw = b["worst"][p][0]
        bw = b["worst"][p][1]
        yw = b["worst"][p][1]
        ta = b["avg"][p][0]
        per = round(b["avg"][p][1], 1)
        print("%d, %.1f, (%d, %d), %.1f, (%d, %d), %.1f, %.1f%%" % (p, tb, bb, yb, tw, bw, yw, ta, per))

g = formatgo(loadgo())
b = loadbatched()
printstats(g, b)
graph(g, b)
