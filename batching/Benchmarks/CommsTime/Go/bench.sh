fyp=../../..

cd $fyp/Benchmarks/CommsTime/Go

for i in 1 2 3 4
do    
    # go test -bench=. -cpu $i -failfast -timeout 0
    go test -bench=. -cpu $i -count 5 -failfast -timeout 0
    # go test -bench=. -cpu $i -count 5 -failfast -timeout 0 -trace traces/trace.$i.out
done
