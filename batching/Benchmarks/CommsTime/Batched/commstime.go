package main

import (
	"fmt"
	"math"
	"runtime"
	"time"
)

func commstime(n int) {
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)
	d := make(chan int)

	go prefix(c, a, 0)
	go delta(a, b, d)
	go succ(b, c)

	consume(d, n)
}

func main() {

	const n = 100000

	const repeat = 5
	varlim := 3000.0
	varmax := 6000.0
	//procs := []int{1, 2, 3, 4}
	procs := []int{4}
	bsz := []uint{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
	ysz := []uint{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25}
	//bsz := []uint{2}
	//ysz := []uint{3}

	best, worst, avg, bb, yb, bw, yw, count := 0.0, 0.0, 0.0, uint(0), uint(0), uint(0), uint(0), 0.0

	print("procs, batchsize, yieldrate, mean duration (ms), variance")

	for _, p := range procs {
		runtime.GOMAXPROCS(p)

		for _, ib := range bsz {
			b := uint(math.Pow(2, float64(ib)))
			runtime.GOBATCHSIZE(b)

			for _, iy := range ysz {
				y := uint(math.Pow(2, float64(iy)))
				if y < b {
					continue
				}
				runtime.GOYIELDRATE(y)

			retake:
				// Take Repeated Readings
				read := make([]float64, repeat)
				// fmt.Print("[")
				for i := 0; i < repeat; i++ {
					t := time.Now() // Start Run
					commstime(n)
					d := time.Since(t) // End Run
					read[i] = float64(d)
					// fmt.Printf("%f, ", read[i]/n)
				}
				// fmt.Print("], ")

				// Calculate Mean
				mean := 0.0
				for _, d := range read {
					mean += float64(d)
				}
				mean /= repeat * n

				// Calculate Variance
				variance := 0.0
				for _, d := range read {
					n := float64(d) / n
					variance += (n - mean) * (n - mean)
				}
				variance /= repeat

				if variance > varlim {
					if variance < varmax {
						// Not variant enough to dismiss yet
						goto retake
					}
				} else {
					fmt.Printf("%d, %d, %d, ", p, b, y)
					fmt.Printf("%f, %f\n", mean, variance)

					if best == 0 || best > mean {
						best, bb, yb = mean, b, y
					}
					if worst == 0 || worst < mean {
						worst, bw, yw = mean, b, y
					}
					avg += mean
					count++
				}
			}
		}

		avg /= count
		fmt.Printf("STATS for p:%d\tBest:(%f,%d,%d), AVG(%f, %d), Worst(%f,%d,%d)\n", p, best, bb, yb, avg, uint(count), worst, bw, yw)
		best, worst, avg, bb, yb, bw, yw, count = 0.0, 0.0, 0.0, uint(0), uint(0), uint(0), uint(0), 0.0

	}
}

func prefix(cin, cout chan int, v int) {
	cout <- v
	for v := range cin {
		cout <- v
	}
}

func id(cin, cout chan int) {
	for v := range cin {
		cout <- v
	}
}

func delta(cin, cout1, cout2 chan int) {
	for v := range cin {
		//go func(v int) { cout1 <- v }(v)
		cout1 <- v
		cout2 <- v
	}
}

func succ(cin, cout chan int) {
	for v := range cin {
		cout <- v + 1
	}
}

func consume(cin chan int, n int) {
	for i := 0; i < n; i++ {
		<-cin
	}
}
