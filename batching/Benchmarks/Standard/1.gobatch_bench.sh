#!/bin/bash
NAME=$1
NUM=$2

fyp=../..
gobatch=$fyp/go.batch/bin/go

TYPE=$NUM-$NAME

# https://golang.org/cmd/go/#hdr-Testing_flags

DIR=./results/$TYPE

BLOCK=block.out
CPU=cpu.out
MEM=mem.out
MUTEX=mutex.out
TRACE=trace.out

OUT=$DIR/profiles

mkdir -p $OUT
  
#GODEBUG=gcstoptheworld=2 
#GODEBUG=gccheckmark=1 

#$gobatch test -bench=. -benchmem -failfast -cpu $i -blockprofile $BLOCK -cpuprofile $CPU -memprofile $MEM -mutexprofile $MUTEX -trace $TRACE -outputdir $OUT >> $DIR/$NAME.traced.bnch.txt
for i in 1 2 3 4
do
$gobatch test -bench=. -benchmem -failfast -cpu $i -timeout 0 -outputdir $OUT >> $DIR/$NAME.bnch.txt
#go test -bench=. -benchmem -failfast -cpu $i -timeout 0 -outputdir $OUT >> $DIR/$NAME.bnch.txt
done

