# Optimising the Go runtime scheduler
## Tharen Abela

This README is included with all source code and documentation that was required by the Final Year Project in Computing Science (ICT3907) for 2018/19.

Below is the Folder Structure of the Dissertation, with all the data gathered and used for the documentation:

```
.
├── Benchmarks
│   ├── CommsTime
│   │   ├── Batched
│   │   │   ├── commstime.batch.bnch.txt
│   │   │   └── commstime.go
│   │   ├── ct_performance.pdf
│   │   ├── Go
│   │   │   ├── bench.sh
│   │   │   ├── commstime.go
│   │   │   ├── commstime.go.bnch.txt
│   │   │   └── commstime_test.go
│   │   └── graph.py
│   ├── Granularity
│   │   ├── bench.sh
│   │   ├── cache_pattern.pdf
│   │   ├── cache_performance.pdf
│   │   ├── cache_speedup.pdf
│   │   ├── granularity.batch.bnch.csv
│   │   ├── granularity.batch.bnch.txt
│   │   ├── granularity.go
│   │   ├── granularity.go.bnch.csv
│   │   ├── granularity.go.bnch.txt
│   │   └── graph.py
│   └── Standard
│       ├── 1.gobatch_bench.sh
│       ├── benchmarks.test
│       ├── binarytree_test.go
│       ├── fannkuch_test.go
│       ├── fasta_test.go
│       ├── fmt_test.go
│       ├── gob_test.go
│       ├── gzip_test.go
│       ├── http_test.go
│       ├── jsondata_test.go
│       ├── json_test.go
│       ├── mandel_test.go
│       ├── parserdata_test.go
│       ├── parser_test.go
│       ├── regexp_test.go
│       ├── results
│       │   ├── 0-go
│       │   │   ├── go.bnch.txt
│       │   │   └── profiles
│       │   ├── 1-batched
│       │   │   ├── batched.bnch.txt
│       │   │   └── profiles
│       │   ├── bench_cmp.sh
│       │   └── go.batched.cmp.txt
│       ├── revcomp_test.go
│       ├── template_test.go
│       └── time_test.go
├── Helper Scripts
│   ├── install.sh
│   ├── reinstall.sh
│   ├── schedtrace.sh
│   └── trace.sh
├── README.md
└── Report
    ├── Dissertation
    │   └── Tharen Abela - Dissertation.pdf
    └── Progress Report
        └── Tharen Abela - Progress Report.pdf
```
15 directories, 47 files
